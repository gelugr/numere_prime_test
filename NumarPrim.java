
package numarprim;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author gelu
 */

public class NumarPrim {
    //Functie care returneaza o lista cu numerele prime gasite in intervalul dat - ca parametri de intrare 
    //returneaza o lista de numere intregi cu numerele prime identificate 
    public static List <Integer> primeNumbers(int number1, int number2){
        List<Integer> primGasit = new ArrayList<>();
        int limitaInferioara, limitaSuperioara;
        int i;
        
        limitaInferioara = (number1 < number2) ? number1 : number2;
        limitaSuperioara = (number1 > number2) ? number1 : number2;
        
        for(i = limitaInferioara; i <= limitaSuperioara; i++) {
            if(estePrim(i)) {
                primGasit.add(i);
            }
        }
        
        return primGasit;
    }
    
    //functia care verifica daca un numar primit ca parametru este prim
    // returneaza true daca numarul este prin
    public static boolean estePrim( int number){
        int i;
        if (number < 2)
            return false;
        for( i = 2; i < number/2 + 1; i++) {
            if(number % i == 0)
            return false;
        }
        return true;
                
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introduceti primul capat al intervalului");
        int numar1 = input.nextInt();
        System.out.println("Introduceti primul capat al intervalului");
        int numar2 = input.nextInt();
        List<Integer> numerePrime = primeNumbers(numar1, numar2);
        
        System.out.println("In intervalul dat au fost identificate " + primeNumbers(numar1, numar2).size() + " numere prime.");
                
        System.out.println("Acestea sunt: ");
//        Afisarea numerelor se face in ordine descrescatoare
        for(int i = numerePrime.size()-1; i >=0; i--) {
             System.out.print(numerePrime.get(i) + "  ");
        }
        
        
    }
    
}
